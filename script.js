/**
 * p5js boilerplate
 * 
 * 
 */

var estado = "intro"; // intro ou jogo
var exibirPressione = true;
var contadorExibirPressione = 0;

var j1, j2, bola;
var cima = false;
var baixo = false;
var deslocamento;
var pontoJ1 = 0
var pontoJ2 = 0
// chamada no inicio do programa
function setup() {
	// cria o canvas com dimensão de 800 x 400
	createCanvas(800, 400);
	//cria a posição inicial de cada objeto e o deslocamento da bolinha (quanto pixels ela vai pular a cada frame)
	j1 = createVector (24, 127);
	bola = createVector(width / 2, height / 2);
	deslocamento = createVector (1, -1);
	noCursor();
}

// chamada toda vez que o quadro for redesenhado
// ou seja, a cada frame da animacao
function draw() {
	if (estado == "intro") {
		background(0);
		textSize(100);
	    fill(255);
	    stroke(0);
	    textAlign(CENTER);
	    text("PONG", width/2, height/2);
	    contadorExibirPressione++;
	    if (contadorExibirPressione == 30) {
		    contadorExibirPressione = 0;
		    exibirPressione = !exibirPressione;
	    }
	    if (exibirPressione) {	    	
		    textSize(20);
		    text("Pressione espaco para iniciar", width/2, height/2 + height/4);
	    }
	} else if (estado == "jogo") {
		j2 = createVector(760, mouseY);
		// pinta o fundo de preto
		background(0,0,0);


	    // circulo
		ellipse(bola.x,bola.y,25);

	    //retangulo 1
		rect(24,j1.y,15,75);

		//retangulo 2
		rect(j2.x,j2.y,15,75);

		// Se a variavel cima for igual a true, o vetor vai perder 5px (Subir)
		if(cima == true){
			j1.y -= 5; //Isso é o mesmo que j1.y = j1.y - 5;
		}
		// Se a variavel baixo for igual a true, o vetor vai ganhar 5px (Descer)
		if(baixo == true){
			j1.y += 5; //Isso é o mesmo que j1.y = j1.y + 5;
		}
		// Adciona o deslocamento a bola (Faz ela se mover)
		bola.add (deslocamento);
		// Roda a bola quando o deslocamento y for menor ou igual a 10
		if (bola.y <= 10 ){
			deslocamento.rotate(HALF_PI);
		}
		// Roda a bola quando o deslocamento y for maior ou igual a 390
		if (bola.y >= 390){
			deslocamento.rotate(HALF_PI);
		}
		// Roda a bola quando o deslocamento x for maior ou igual a 790
		if (bola.x >= 790){
			bola.x = width / 2;
			bola.y = height / 2;
			pontoJ1 ++;
			deslocamento.x = 1;
			deslocamento.y = -1;

		}
		// Roda a bola quando o deslocamento x for menor ou igual a 10
		if(bola.x <= 10){
			bola.x = width / 2;
			bola.y = height / 2;
			pontoJ2 ++;
			deslocamento.x = 1;
			deslocamento.y = -1;
		}

		if (bola.y > j1.y && bola.y < j1.y + 75 && bola.x < j1.x + 27){
			deslocamento.rotate(HALF_PI);
		}
		if (bola.y > j2.y && bola.y < j2.y + 75 && bola.x > j2.x - 10){
			deslocamento.rotate(HALF_PI);
		}

		textSize(60);
	    fill(255);
	    stroke(0);
	    text(pontoJ1, width/15, height/6);
	    text(pontoJ2, 700, height/6);

		//Aumenta a velocidade da bola
		deslocamento.mult(1.0005);
	}
}	
function keyPressed() {
	if (estado == "intro" && keyCode == 32) estado = "jogo";
	// Se a tecla apertada for UP_ARROW (Código para a tecla de setinha pra cima), a variavel cima vai ganhar o valor true
	if (keyCode == UP_ARROW){
		cima = true;
	}
	// Se a tecla apertada for DOWN_ARROW (Código para a tecla de setinha pra baixo), a variavel baixo vai ganhar o valor true
	if (keyCode == DOWN_ARROW){
		baixo = true;
	}
	if(keyCode == 82){
		pontoJ1 = 0;
		pontoJ2 = 0;
	}
}
function keyReleased() {
	// Se a tecla apertada for UP_ARROW (Código para a tecla de setinha pra cima), a variavel cima vai ganhar o valor false
	if (keyCode == UP_ARROW){
		cima = false;
	} 
	// Se a tecla apertada for DOWN_ARROW (Código para a tecla de setinha pra baixo), a variavel baixo vai ganhar o valor false
	if (keyCode == DOWN_ARROW){
		baixo = false;
	}
}

